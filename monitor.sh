#!/bin/bash

CURRENTDATE=`date +"%D %T"`
declare -a StringArray=("heise.de" "tvnow.de" "debian.org" "www.kame.net" "facebook.com" "fast.com")

curl_format=";%{time_namelookup};%{time_total}"

for val in ${StringArray[@]}; do
	echo $val
	logfile="/home/jan/monitor/$val.localdns.log"
	echo -n "${CURRENTDATE}" >> $logfile
	curl $val -w$curl_format -o /dev/null 1>>$logfile 2>/dev/null
	if [ $? -eq 0 ]
	then
		echo -n ";1" >> $logfile
	else
		echo -n ";0" >> $logfile
	fi

	curl $val -6 -w$curl_format -o /dev/null 1>>$logfile 2>/dev/null
	if [ $? -eq 0 ]
	then
		echo -n ";1" >> $logfile
	else
		echo -n ";0" >> $logfile
	fi

	PING4=`ping -c4 -4 $val | grep 'rtt' | awk -F'=' '{print $2}' | awk -F'/' '{ print $2}' | grep . && echo -n "" || echo -n "-1"`
	PING6=`ping -c4 -6 $val | grep 'rtt' | awk -F'=' '{print $2}' | awk -F'/' '{ print $2}' | grep . && echo -n "" || echo -n "-1"`
	echo ";${PING4};${PING6}" >> $logfile
done


