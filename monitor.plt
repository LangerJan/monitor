#!/usr/bin/env gnuplot

set xdata time
set timefmt "%m/%d/%y"
#set xrange ["03/21/95":"03/22/95"]
#set yrange [300:2100]
set format x "%H:%M"
set timefmt "%m/%d/%y %H:%M:%S"
set terminal png size 1024,768
set datafile separator ";"

set ytics nomirror
set y2tics
set y2range [0:1]

set output "debian.org.localdns.v4.png"
plot "debian.org.localdns.log" using 1:4 with boxes fs solid 0.25 lc rgb "#00FF00" t "success" axis x1y2,\
     "debian.org.localdns.log" using 1:3 with linespoints t "time dns", \
     "debian.org.localdns.log" using 1:8 with linespoints t "ping"

set output "debian.org.localdns.v6.png"
plot "debian.org.localdns.log" using 1:7 with boxes fs solid 0.25 lc rgb "#00FF00" t "success" axis x1y2,\
     "debian.org.localdns.log" using 1:6 with linespoints t "time dns", \
     "debian.org.localdns.log" using 1:9 with linespoints t "ping"
